#include "token.h"

class Scanner 
{
    private: 
        string input;//Armazena o texto de entrada
        int pos;//Posição atual
        int lineNum;
    
    public:
    //Construtor
        Scanner(string);
        ~Scanner();
    
        //Método que retorna o próximo token da entrada
        Token* nextToken();     

        // Método que retorna o número da linha
        int getLine();
    
        //Método para manipular erros
        void lexicalError(string);
};

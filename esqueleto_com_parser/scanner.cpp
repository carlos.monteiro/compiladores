#include "scanner.h"
#include <iostream>
#include <fstream>
using namespace std;

//Construtor que recebe uma string com o nome do arquivo 
//de entrada e preenche input com seu conteúdo.
Scanner::Scanner(string input/*, SymbolTable* table*/)
{
    /*this->input = input;
    cout << "Entrada: " << input << endl << "Tamanho: " 
         << input.length() << endl;*/
    pos = 0;
    lineNum = 1;

    //st = table;

    ifstream inputFile(input, ios::in);
    string line;

    if (inputFile.is_open())
    {
        while (getline (inputFile,line) )
        {
            this->input.append (line + '\n');
        }
        inputFile.close();
    }
    else 
        cout << "Unable to open file\n"; 

    //A próxima linha deve ser comentada posteriormente.
    //Ela é utilizada apenas para verificar se o 
    //preenchimento de input foi feito corretamente.
    cout << this->input;

}

int 
Scanner::getLine()
{
    return lineNum;
}

//Método que retorna o próximo token da entrada
Token* 
Scanner::nextToken()
{
    Token* tok;
    string lexeme;
    string msg = "ERRO";

    //Consome espaços em branco
    
    while (isspace(input[pos]))
        pos++;

    // Verificar os tokens possíveis
    switch(input[pos])
    {
        case '\0':
            return new Token(END_OF_FILE);

        case '\n':
            lineNum++;
            pos = 0;
        
        case '\r':
        case '\t':
        case '\f':
            pos++;

        // Operadores relacionais //
        case '&':
            pos++;
            if (input[pos] == '&')
                return new Token(RELOP, AND);
            lexicalError("&");

        case '<':
            pos++;
            return new Token(RELOP, LT);

        case '>':
            pos++;
            return new Token(RELOP, GT);

        case '=':
            pos++;
            if (input[pos] == '=')
            {
                pos++;
                return new Token(RELOP, EQ);
            }
            return new Token(RELOP, ASSIG); 


        case '!':
            pos++;
            if(input[pos] == '=')
            {
                pos++;
                return new Token(RELOP, NE);
            }
            return new Token(RELOP, NOT);

        // Operadores aritméticos //
        case '+':
            pos++;
            return new Token(ARITMOP, PL);

        case '-':
            pos++;
            return new Token(ARITMOP, MN);
        
        case '*':
            pos++;
            return new Token(ARITMOP, MUL);
        
        case '/':
            pos++;
            if(input[pos] == '*'){
                pos++;
                int loop = 0;
                while(loop == 0){
                    if (input[pos] == '\0')
                    {   
                        lexicalError(msg);
                        tok = new Token(END_OF_FILE);
                        return tok;
                    }
                    if (input[pos] == '\n')
                    {
                        lineNum += 1;
                        pos = 0;
                    }
                    if(input[pos] == '*'){
                        pos++;
                        if(input[pos] == '/'){
                            loop = 1;
                            pos++;
                        }
                    }
                    pos++;

                }
                while (isspace(input[pos]))
                    pos++;
            }
            else if(input[pos] == '/'){
                while (input[pos] != '\0'){
                    pos++;
                }
                tok = new Token(END_OF_FILE);
                return tok;
            }

            else{
                tok = new Token(OP, DIV);
                return tok;
            }

        // Separadores
        case '(':
            pos++;
            return new Token(SEP, LROUND);
        case ')':
            pos++;
            return new Token(SEP, RROUND);
        case '[':
            pos++;
            return new Token(SEP, LSQUARE);
        case ']':
            pos++;
            return new Token(SEP, RSQUARE);
        case '{':
            pos++;
            return new Token(SEP, LBRACE);
        case '}':
            pos++;
            return new Token(SEP, RBRACE);
        case ';':
            pos++;
            return new Token(SEP, SCOLON);
        case '.':
            pos++;
            return new Token(SEP, PERIOD);
        case ',':
            pos++;
            return new Token(SEP, COMMA);
    }

    //Identificadores
    if (isalpha(input[pos]))
    {
        lexeme.push_back(input[pos]);
        pos++;

        while (isalnum(input[pos]) || input[pos] == '_')
        {
            lexeme.push_back(input[pos]);
            pos++;
            if((lexeme == "System" || lexeme == "System.out") && input[pos] == '.'){
                lexeme.push_back(input[pos]);
                pos++;
            }
        }
        
        //Busco na tabela para ver se é palavra reservada
        //tok = searchTable(lexeme);
        if (!tok)
            tok = new Token(ID);
    
        return tok;
    }

    
    //Números
    if (isdigit(input[pos]))
    {
        pos++;
        while (isdigit(input[pos]))
            pos++;

        tok = new Token(NUMBER, INTEGER_LITERAL);
        
        return tok;
    }

    lexicalError(msg);
    
    tok = new Token(UNDEF);

    return tok;
 
}

void 
Scanner::lexicalError(string msg)
{
    cout << "Linha "<< lineNum << ": " << msg << endl;
    
    exit(EXIT_FAILURE);
}

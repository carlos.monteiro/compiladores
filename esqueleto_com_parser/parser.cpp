#include "parser.h"

string* vet;

using namespace std;

void print(Token*);
void allocVetor();


Parser::Parser(string input)
{
	//currentST = globalST = new SymbolTable();
	//initSimbolTable();

	scanner = new Scanner(input/*, globalST*/);
}

void
Parser::advance()
{
	lToken = scanner->nextToken();
	//print(lToken);
	cout << lToken->name << " PP ";
	 
}

void
Parser::match(int t)
{
	if (lToken->name == t || lToken->attribute == t)
		advance();
	else
		error("Erro inesperado");
}

void
Parser::run()
{
	advance();	

	program();
	
	cout << "Compilação encerrada com sucesso!\n";
}

void
Parser::program()
{
	//TODO
    mainClass();
    while(lToken->name == CLASS){
        classDeclaration();
        advance();
    }
    match(END_OF_FILE);
}


void Parser::mainClass(){
    match(CLASS);
    match(ID);
    match(LBRACE);
    match(PUBLIC);
    match(STATIC);
    match(VOID);
    match(MAIN);
    match(LROUND);
    match(STRING);
    match(LSQUARE);
    match(RSQUARE);
    match(RROUND);
    match(ID);
    match(LBRACE);
    statement();
    match(RBRACE);
    match(RBRACE);
}

void Parser::classDeclaration(){
    match(CLASS);
    match(ID);
    if(lToken->name == EXTENDS){
        match(ID);
    }
    match(LBRACE);

    // Sequência de varDeclaration
    while (lToken->attribute == INT || 
           lToken->attribute == BOOLEAN || 
           lToken->name == ID && lToken->attribute == UNDEF){
                varDeclaration();
            }
    
    // Sequência de methodDeclaration
    while (lToken->attribute == PUBLIC){
                methodDeclaration();
            }
    
    match(RBRACE);
}

void Parser::varDeclaration(){
    Type();
    match(ID);
    match(SCOLON);
}

void Parser::methodDeclaration(){
    match(PUBLIC);
    Type();
    match(ID);
    match(LROUND);
    if (lToken->attribute == INT || 
        lToken->attribute == BOOLEAN || 
        lToken->name == ID && lToken->attribute == UNDEF){
            advance();
            match(ID);
            while (lToken->name == COMMA){
                Type();
                match(ID);
            }
        }

    match(RROUND);
    match(LBRACE);

    // Sequência de varDeclaration
    while (lToken->attribute == INT || 
           lToken->attribute == BOOLEAN || 
           lToken->name == ID && lToken->attribute == UNDEF){
                varDeclaration();
            }
    
    // Sequência de statement
    while (lToken->name == LBRACE || 
           lToken->attribute == IF || 
           lToken->attribute == WHILE ||
           lToken->attribute == SYSTEMOUTPRINTLN ||
           lToken->name == ID && lToken->attribute == UNDEF){
                statement();
            }
    match(RETURN);
    expression();
    match(SCOLON);
    match(RBRACE);
}

void Parser::statement(){
    switch(lToken->name)
    {
        case LBRACE:
            advance();
            // Sequência de statement
            while (lToken->name == LBRACE || 
                    lToken->attribute == IF || 
                    lToken->attribute == WHILE ||
                    lToken->attribute == SYSTEMOUTPRINTLN ||
                    lToken->name == ID && lToken->attribute == UNDEF){
                        statement();
                    }
            match(RBRACE);

        case IF:
            advance();
            match(LROUND);
            expression();
            match(RROUND);
            statement();
            match(ELSE);
            statement();

        case WHILE:
            advance();
            match(LROUND);
            expression();
            match(RROUND);
            statement();

        case SYSTEMOUTPRINTLN:
            advance();
            match(LROUND);
            expression();
            match(RROUND);
            match(SCOLON);

        case ID:
            advance();
            if(lToken->name == EQ){
                expression();
                match(SCOLON);
            }
            else if(lToken->name == LSQUARE){
                expression();
                match(RSQUARE);
                match(EQ);
                expression();
                match(SCOLON);
            }
    }
}

void Parser::Type(){
    switch (lToken->name)
    {
        case INT:
            advance();
            if (lToken->name == LSQUARE)
                match(RSQUARE); 

        case BOOLEAN:
            advance();
        
        case ID:
            advance();

    }
}

void Parser::expression(){
    switch (lToken->name)
    {
        case INTEGER_LITERAL:
        case TRUE:
        case FALSE:
        case ID:
        case THIS:
            expression_();
        
        case NEW:
            advance();

            if (lToken->name == INT){
                advance();
                if (lToken->name == LSQUARE){
                    expression();
                    match(RSQUARE);
                    expression_();
                } 
            } else if (lToken->name == ID) {
                if (lToken->name == LBRACE)
                {
                    match(RBRACE);
                    expression_();
                }
            }

        case NOT:
            expression();
            expression_();
        
        case LBRACE:
            expression();
            match(RBRACE);
            expression_();
    }
}

void Parser::expression_(){
    switch (lToken->name)
    {
        case OP:
            expression();
            expression_();
        
        case LSQUARE:
            expression();
            match(LSQUARE);
            expression_();
        
        case PERIOD:
            advance();
            if (lToken->name == LENGTH)
                expression_;
            else if(lToken->name == ID)
            {
                match(LROUND);
                if (lToken->attribute == INTEGER_LITERAL ||
                    lToken->attribute == TRUE ||
                    lToken->attribute == FALSE ||
                    lToken->attribute == THIS ||
                    lToken->attribute == NEW ||
                    lToken->attribute == NOT ||
                    lToken->attribute == LROUND ||
                    lToken->name == ID && lToken->attribute == UNDEF){
                        expression();
                        while (lToken->name == COMMA)
                        {
                            expression();
                        }
                    }
            }

        default:
            advance();

    }
}

void Parser::op(){
    switch (lToken->name)
    {
        case AND:
        case LT:
        case GT:
        case EQ:
        case NE:
        case PL:
        case MN:
        case MUL:
        case DIV:
            advance();
    }
}


//Continuar....

void
Parser::error(string str)
{
	cout << "Linha " << scanner->getLine() << ": " << str << endl;

	exit(EXIT_FAILURE);
}


void allocVetor()
{
    vet = new string[35];

    vet[0] = "UNDEF";//0
    vet[1] = "ID";//1
    vet[2] = "IF";//2
    vet[3] = "ELSE";//3
    vet[4] = "RELOP"; //5
    vet[5] = "EQ";//6
    vet[6] = "NE";//7
    vet[7] = "GT";//8
    vet[8] = "GE";//9
    vet[9] = "LT";//10
    vet[10] = "LE";//11
    vet[11] = "NUMBER";//12
    vet[12] = "DOUBLE_LITERAL";//13
    vet[13] = "FLOAT_LITERAL";//14
    vet[14] = "INTEGER_LITERAL";//15
    vet[15] = "END_OF_FILE";//16
    vet[16] = "SEP";
    vet[17] = "OP";
    vet[18] = "BOOLEAN";
    vet[19] = "CLASS";
    vet[20] = "EXTENDS";
    vet[21] = "FALSE";
    vet[22] = "INT";
    vet[23] = "LENGTH";
    vet[24] = "MAIN";
    vet[25] = "NEW";
    vet[26] = "PUBLIC";
    vet[27] = "RETURN";
    vet[28] = "STATIC";
    vet[29] = "STRING";
    vet[30] = "SYSTEMOUTPRINTLN";
    vet[31] = "THIS";
    vet[32] = "TRUE";
    vet[33] = "VOID";
    vet[34] = "WHILE";

}


void print(Token* t)
{
    cout << vet[t->name];

    if (t->attribute != UNDEF)
        cout << "(" << vet[t->attribute] << ")";

    cout << " ";
}

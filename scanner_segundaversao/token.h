#include <iostream>
#include <ctype.h>//Funções de caracteres
#include <string>

using namespace std;

enum Names 
{
    UNDEF,//0
    ID,//1
    IF,//2
    ELSE,//3
    RELOP,//5
    EQ,//6
    NE,//7
    GT,//8
    GE,//9
    LT,//10
    LE,//11
    NUMBER,//12
    DOUBLE_LITERAL,//13
    FLOAT_LITERAL,//14
    INTEGER_LITERAL,//15
    END_OF_FILE,//16
    SEP,//17
    OP,//18
    BOOLEAN,//19
    CLASS,//20
    EXTENDS,//21
    FALSE,//22
    INT,//23
    LENGTH,//24
    MAIN,//25
    NEW,//26
    PUBLIC,//27
    RETURN,//28
    STATIC,//29
    STRING,//30
    SYSTEMOUTPRINTLN,//31
    THIS,//32
    TRUE,//33
    VOID,//34
    WHILE//35
};

class Token 
{
    public: 
        int name;
        int attribute;
        string lexeme;
    
        Token(int name, string lexeme)
        {
            this->name = name;
            this->lexeme = lexeme;
            attribute = UNDEF;
        }

        Token(int name)
        {
            this->name = name;
            attribute = UNDEF;
        }
        
        Token(int name, int attr)
        {
            this->name = name;
            attribute = attr;
        }
};
#include "scanner.h"   
 
using namespace std;

Token** table;

void initTable()
{
    table = new Token*[19];

    table[0] = new Token(IF, "if");
    table[1] = new Token(ELSE, "else");
    table[2] = new Token(BOOLEAN, "boolean");
    table[3] = new Token(CLASS, "class");
    table[4] = new Token(EXTENDS, "extends");
    table[5] = new Token(FALSE, "false");
    table[6] = new Token(INT, "int");
    table[7] = new Token(LENGTH, "length");
    table[8] = new Token(MAIN, "main");
    table[9] = new Token(NEW, "new");
    table[10] = new Token(PUBLIC, "public");
    table[11] = new Token(RETURN, "return");
    table[12] = new Token(STATIC, "static");
    table[13] = new Token(STRING, "String");
    table[14] = new Token(SYSTEMOUTPRINTLN, "System.out.println");
    table[15] = new Token(THIS, "this");
    table[16] = new Token(TRUE, "true");
    table[17] = new Token(VOID, "void");
    table[18] = new Token(WHILE, "while");
}

void freeTable()
{
    for (int i = 0; i < 19; i++)
        delete table[i];

    delete[] table;
}

Token* searchTable(string lexeme)
{
    for (int i = 0; i < 19; i++)
        if (lexeme == table[i]->lexeme)
            return table[i];

    return 0;
}

//Construtor
Scanner::Scanner(string input)
{
    initTable();
    this->input = input;
    cout << "Entrada: " << input << endl << "Tamanho: " << input.length() << endl;
    pos = 0;
}

Scanner::~Scanner()
{
    freeTable();
}

//Método que retorna o próximo token da entrada
Token* 
Scanner::nextToken()
{
    Token* tok;
    string lexeme;

    //Verifica se chegou ao final do arquivo
    if (input[pos] == '\0')
    {
        tok = new Token(END_OF_FILE);

        return tok;
    }

    //Consome espaços em branco
    
    while (isspace(input[pos]))
        pos++;

    if(input[pos] == '/'){
        pos++;
            if(input[pos] == '*'){
                pos++;
                int loop = 0;
                while(loop == 0){
                    if (input[pos] == '\0')
                    {   
                        lexicalError();
                        tok = new Token(END_OF_FILE);
                        return tok;
                    }
                    if(input[pos] == '*'){
                        pos++;
                        if(input[pos] == '/'){
                            loop = 1;
                            pos++;
                        }
                    }
                    pos++;

                }
                while (isspace(input[pos]))
                    pos++;
            }
            else if(input[pos] == '/'){
                while (input[pos] != '\0'){
                    pos++;
                }
                tok = new Token(END_OF_FILE);
                return tok;
            }

            else{
                tok = new Token(OP);
                return tok;
            }
    }

    //Identificadores
    if (isalpha(input[pos]))
    {
        lexeme.push_back(input[pos]);
        pos++;

        while (isalnum(input[pos]))
        {
            lexeme.push_back(input[pos]);
            pos++;
            if((lexeme == "System" || lexeme == "System.out") && input[pos] == '.'){
                lexeme.push_back(input[pos]);
                pos++;
            }
        }
        

        //Busco na tabela para ver se é palavra reservada
        tok = searchTable(lexeme);
        if (!tok)
            tok = new Token(ID);
    
        return tok;
    }

    
    //Números
    if (isdigit(input[pos]))
    {
        pos++;

        while (isdigit(input[pos]))
            pos++;

        bool isFloat = false;

        if (input[pos] == '.')
        {
            pos++;

            if (isdigit(input[pos]))
            {
                pos++;

                while (isdigit(input[pos]))
                    pos++;
            }
            else
                lexicalError();

            isFloat = true;
        }
                       
        if (input[pos] == 'E')
        {
            pos++;

            if (input[pos] == '+' || input[pos] == '-')
                pos++;
            
            if (isdigit(input[pos]))
            {
                pos++;
                while (isdigit(input[pos]))
                    pos++;
            }
            else
                lexicalError();

            tok = new Token(NUMBER, DOUBLE_LITERAL);           
        }
        else if (isFloat)
            tok = new Token(NUMBER, FLOAT_LITERAL);
        else
            tok = new Token(NUMBER, INTEGER_LITERAL);
        
        return tok;
    }
    
    if(input[pos] == '(' || input[pos] == ')' || input[pos] == '[' || input[pos] == ']' || input[pos] == '{' || input[pos] == '}' || input[pos] == ';' || input[pos] == '.' || input[pos] == ',' ){
        tok = new Token(SEP);
        pos++;
        return tok;
    }
    if(input[pos] == '&'){
        pos++;
        if (input[pos] == '&'){
            tok = new Token(OP);
            pos++;
            return tok;
        }
        else{
            tok = new Token(OP);
            return tok;
        }
    }
    
    if(input[pos] == '<' || input[pos] == '>' || input[pos] == '+' || input[pos] == '-' || input[pos] == '*'){
        pos++;
        tok = new Token(OP);
        return tok;
    }


    if(input[pos] == '='){
        pos++;
        if (input[pos] == '='){
            tok = new Token(OP);
            pos++;
            return tok;
        }
        else{
            tok = new Token(OP);
            return tok;
        }
    }

    if(input[pos] == '!'){
        pos++;
        if (input[pos] == '='){
            tok = new Token(OP);
            pos++;
            return tok;
        }
        else{
            tok = new Token(OP);
            return tok;
        }
    }
        
    if(input[pos] == '\r' || input[pos] == '\t' || input[pos] == '\f' || input[pos] == '\n'){
        pos++;
    }

    lexicalError();
    
    tok = new Token(UNDEF);

    return tok;
}

void 
Scanner::lexicalError()
{
    cout << "Token mal formado\n";
    
    exit(EXIT_FAILURE);
}

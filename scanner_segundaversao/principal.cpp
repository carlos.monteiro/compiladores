#include "scanner.h"

string* vet;

using namespace std;

void print(Token*);
void allocVetor();
void freeVetor();

int main(int argc, char* argv[]) 
{
    string input;

    getline(cin, input);
    std:cout << input << endl;
    Scanner* scanner = new Scanner(input);

    allocVetor();
    
    Token* t;
    
    do
    {
        t = scanner->nextToken();
        
        print(t);
    }while (t->name != END_OF_FILE);

    delete scanner;

    return 0;
}

void allocVetor()
{
    vet = new string[35];

    vet[0] = "UNDEF";//0
    vet[1] = "ID";//1
    vet[2] = "IF";//2
    vet[3] = "ELSE";//3
    vet[4] = "RELOP"; //5
    vet[5] = "EQ";//6
    vet[6] = "NE";//7
    vet[7] = "GT";//8
    vet[8] = "GE";//9
    vet[9] = "LT";//10
    vet[10] = "LE";//11
    vet[11] = "NUMBER";//12
    vet[12] = "DOUBLE_LITERAL";//13
    vet[13] = "FLOAT_LITERAL";//14
    vet[14] = "INTEGER_LITERAL";//15
    vet[15] = "END_OF_FILE";//16
    vet[16] = "SEP";
    vet[17] = "OP";
    vet[18] = "BOOLEAN";
    vet[19] = "CLASS";
    vet[20] = "EXTENDS";
    vet[21] = "FALSE";
    vet[22] = "INT";
    vet[23] = "LENGTH";
    vet[24] = "MAIN";
    vet[25] = "NEW";
    vet[26] = "PUBLIC";
    vet[27] = "RETURN";
    vet[28] = "STATIC";
    vet[29] = "STRING";
    vet[30] = "SYSTEMOUTPRINTLN";
    vet[31] = "THIS";
    vet[32] = "TRUE";
    vet[33] = "VOID";
    vet[34] = "WHILE";

}

void freeVetor()
{
    delete[] vet;
}

void print(Token* t)
{
    cout << vet[t->name];

    if (t->attribute != UNDEF)
        cout << "(" << vet[t->attribute] << ")";

    cout << " ";
}